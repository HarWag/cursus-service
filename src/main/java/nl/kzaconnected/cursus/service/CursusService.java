package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.configuration.ResourceNotFoundException;
import nl.kzaconnected.cursus.model.Dao.*;
import nl.kzaconnected.cursus.model.Dto.CursusDto;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class CursusService extends AbstractCursusService {

    public CursusDto findOne(Long id) {
        Cursus cursus = cursusRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);
        CursusDto cursusDto = convertToDto(cursus);
        Collections.sort(cursusDto.getCursusdata(), Comparator.comparing(Datum::getDatum));
        return cursusDto;
        }

    public List<CursusDto> findAll() {
        List<Cursus> cursussen = cursusRepository.findAll();
        List<CursusDto> cursusDtos = convertToDtoList(cursussen);
        for (CursusDto cursusDto : cursusDtos){
            Collections.sort(cursusDto.getCursusdata(), Comparator.comparing(Datum::getDatum));
        }
        Collections.sort(cursusDtos, Comparator.comparing(c-> c.getCursusdata().get(0).getDatum()));
        return cursusDtos;
    }

    public CursusDto save(CursusDto cursusDto){
        Attitude attitude = attitudeRepository.findByAttitude(cursusDto.getAttitude());
        Functieniveau functieniveau = functieniveauRepository.findByFunctieniveau(cursusDto.getFunctieniveau());
        Slagingscriterium slagingscriterium = slagingscriteriumRepository.findBySlagingscriterium(cursusDto.getSlagingscriterium());
        Status status = statusRepository.findByStatus(cursusDto.getStatus());
        Cursus cursus = convertToDao(cursusDto);
        cursus.getAttitude().setId(attitude.getId());
        cursus.getFunctieniveau().setId(functieniveau.getId());
        cursus.getSlagingscriterium().setId(slagingscriterium.getId());
        cursus.getStatus().setId(status.getId());
        cursus = cursusRepository.save(cursus);
        return convertToDto(cursus);
    }

    public void delete(Long id) {
        cursusRepository.delete(
                cursusRepository.findById(id)
                        .orElseThrow(ResourceNotFoundException::new));
    }
}
